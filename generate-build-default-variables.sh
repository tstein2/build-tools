#!/bin/bash
echo "  _              _              _       "
echo " | |_ ___  ___  | |_ ___   ___ | |___   "
echo " | __/ __|/ __| | __/ _ \ / _ \| / __|  "
echo " | |_\__ \ (__  | || (_) | (_) | \__ \  "
echo "  \__|___/\___|  \__\___/ \___/|_|___/  "
                                      
env

if [ -z "$CI_COMMIT_TAG" ]
then
    if [ $CI_BUILD_REF_NAME = "master" ]
    then 
        echo "it's master build ..."
        export RELEASE_KEY=latest
    else
        echo "it's a feature build ..."
        export RELEASE_KEY=$(echo $CI_COMMIT_REF_NAME | sed "s/[^[[:alnum:]]/-/g" | tr "[:upper:]" "[:lower:]")
    fi
else
    echo "It's a tag build ..."
    export RELEASE_KEY=$CI_COMMIT_TAG
fi

export CONTAINER_RELEASE_IMAGE=$(echo $CI_REGISTRY_IMAGE)
export CONTAINER_RELEASE_TAG=$(echo $CONTAINER_RELEASE_IMAGE:$RELEASE_KEY)
